# 2DPlatformerBaseMovement



## Description

This project is a stripped down version of the base movement controller I use in my game: **Kindled**.

List of included 2D Platformer mechanical systems:
- **Coyote Time**: Extra time after walking off a ledge to still accept a jump input.
- **Jump Buffering**: Automatically trigger a jump if the player presses jump a few frames before hitting the ground.
- **Variable Gravity**: Gravity is weaker when rising but stronger when falling. This is overrided in certain scenarios.
- **Jump Cutting**: If the player releases jump while rising, cut the jump short. I do this by halving current Y velocity and simultaneously increasing downward gravity by a large amount for a short duration.
- **Variable Acceleration**: Force is applied via lerps to increase or decrease velocity when traveling in the X direction. There are various constants that are used dependent on situation. Most notably, a smaller one is used if accelerating in the same direction of motion but a stronger acceleration is imposed when changing direction. Additionally, I lerp the acceleration value itself when it changes to a smaller number but set it immediately to a new value whenever a greater acceleration is set.
- **Momentum Preservation**: When no X input is detected or the current X input is in the direction of motion, preserve current momentum by dampening the rate of deceleration.

List of mechanical systems **not** included in this project:
- **Bump Shifting/Edge Detection**: If the player would hit their head on a corner, but they have 2/3rds or more clearance, nudge their position over enough to not interrupt their jump.
- **Apex Modifiers**: Grant the player a moment of anti-gravity and a small burst of speed at the apex of their jump.
- **Max Jump**: When a jump is performed, holding jump the whole time will lock the downward gravity at the weaker “rising gravity” for the full jump duration to perform a continuous arc.

Less standard included mechanical systems that might catch you off guard:
- **Instant Turn**: I track the "most recent horizontal" input and use that when computing velocity in the X direction. This prevents the player from stopping if both Left and Right inputs are pressed simultaneously (something that's seen in most games of this nature). I prefer this quicker change of direction but that may just be me. It's also worth noting that such an action is impossible with a joystick, so this could imply a bridging of that gap between M&K and controller X inputs.

## Godot Version

Godot *4.2.1* was used for the creation of this project.

## Using this Code

Feel free to credit me if you use this code in your project, however I don't believe I'm doing anything
particularly revolutionary. This is just my own implementation of standard 2D Platformer movement systems.

Also, please use a State Machine of some variety if you do. This code was stripped for demonstrational purposes.

Xerako