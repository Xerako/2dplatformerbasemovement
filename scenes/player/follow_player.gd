extends Marker2D

@onready var player : Player = $"../Player"
@onready var cam : Camera2D = $"../Camera2D"

func _physics_process(delta):
	global_position = global_position.lerp(player.global_position, 6 * delta)
	#cam.global_position = cam.global_position.lerp(global_position, 10 * delta)
	cam.global_position = global_position
