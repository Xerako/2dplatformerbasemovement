extends CharacterBody2D
class_name Player

# timers
@onready var jump_timer : Timer = $Timers/JumpTimer
@onready var jump_cut_timer : Timer = $Timers/JumpCutTimer
@onready var jump_cut_grav_timer : Timer = $Timers/JumpCutGravTimer
@onready var floor_timer : Timer = $Timers/FloorTimer

# timer related bools
var jumping : bool = false :
	get:
		jumping = not jump_timer.is_stopped()
		return jumping
var grounded : bool = false :
	get:
		grounded = not floor_timer.is_stopped()
		return grounded
var jump_cut : bool = false :
	get:
		jump_cut = not jump_cut_timer.is_stopped()
		return jump_cut
var jump_cut_grav : bool = false :
	get:
		jump_cut_grav = not jump_cut_grav_timer.is_stopped()
		return jump_cut_grav

# velocity variables
var new_velocity : Vector2 = Vector2.ZERO
var target_vel_x : float = 0
var target_vel_y : float = 0

# player stat variables
var speed : float = 110
var jump_speed : float = 290
var grav_up : float = 550
var grav_down : float = 650
var grav_cut : float = grav_down * 1.5
var acceleration : float = 12
var deceleration : float = 15
var terminal_velocity : float = 240

# dynamic variables used by functions
var grav : float
var force : float

# player input variables
var horizontal : int
var horiz_right : int
var horiz_left : int
var most_recent_horiz : int

func _process(_delta) -> void:
	update_inputs()

func _physics_process(delta) -> void:
	apply_gravity(delta)
	move(delta)

func update_inputs() -> void:
	horiz_left = int(Input.is_action_pressed("left"))
	horiz_right = int(Input.is_action_pressed("right"))
	# if both right and left are pressed at the same time
	# set horizontal direction to follow the most recent key press
	horizontal = horiz_right - horiz_left
	if horiz_right + horiz_left == 1:
		most_recent_horiz = horiz_right - horiz_left
	elif horiz_right + horiz_left:
		horizontal = -most_recent_horiz
	
	if is_on_floor():
		floor_timer.start()
	if Input.is_action_just_pressed("jump"):
		jump_timer.start()
		jump_cut_timer.stop()
	if Input.is_action_just_released("jump"):
		jump_cut_timer.start()

func move(delta: float) -> void:
	compute_vx(delta)
	compute_vy(delta)
	set_velocity(new_velocity)
	move_and_slide()
	new_velocity = velocity

func compute_vx(delta: float) -> void:
	# set target velocity for x direction
	target_vel_x = speed * horizontal
	
	# set force based on user input
	if horizontal == 0:
		force = deceleration
	else:
		force = acceleration
		# amplify force if accelerating in opposite direction of current velocity
		if absf(new_velocity.x) > 0.01 && sign(new_velocity.x) != sign(horizontal):
			force = force * 2.0
	
	# dampen force when in air
	if !grounded:
		force = force * 0.85
	
	# momentum preservation logic when applying forces
	# this essentially uses a lower deceleration force if airborne and no horizontal input is detected
	if (sign(new_velocity.x) == sign(horizontal) || (horizontal == 0 && !grounded)) && absf(new_velocity.x) > absf(target_vel_x):
		new_velocity.x = lerpf(new_velocity.x, target_vel_x, 2.5 * delta)
	else:
		new_velocity.x = lerpf(new_velocity.x, target_vel_x, force * delta)

func compute_vy(_delta: float) -> void:
	# accept a jump if grounded
	if grounded && jumping:
		jump_timer.stop()
		jump_cut_grav_timer.stop()
		new_velocity.y = -jump_speed
	
	# accept a jump cut only when rising
	if new_velocity.y < 0 && jump_cut:
		jump_cut_timer.stop()
		jump_cut_grav_timer.start()
		new_velocity.y = new_velocity.y * 0.5

func apply_gravity(delta: float) -> void:
	# set grav based on vertical velocity
	if new_velocity.y <= 0:
		grav = grav_up
	else:
		grav = grav_down
	
	# override grav with amplified jump cut gravity when necessary
	if jump_cut_grav:
		grav = grav_cut
	
	# apply gravity
	new_velocity.y += grav * delta
	# clamp falling velocity
	new_velocity.y = clampf(new_velocity.y, -jump_speed * 10, terminal_velocity)
